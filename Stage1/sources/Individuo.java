

public class Individuo {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;

    public Individuo (Comuna comuna, double speed, double deltaAngle){
        x=Math.random()*comuna.getWidth();
        y=Math.random()*comuna.getHeight();
        angle = Math.random()*2*Math.PI;
        this.comuna=comuna;
        this.speed=speed;
        this.deltaAngle=deltaAngle;
    }

    public static String getStateDescription(){
        return "x,\ty";
    }
    public String getState() {
        return x + ",\t" + y;
    }
    public void computeNextState(double delta_t) {
        double r=Math.random();
        double alpha=deltaAngle*(1-2*Math.round(r));//suma o resta aleatoriamente el deltaAngle
        angle+= alpha;
        if (angle>2*Math.PI){
            angle-=2*Math.PI;
        }
        x_tPlusDelta=x+speed*Math.cos(angle);
        if(x_tPlusDelta < 0){   // rebound logic
            x_tPlusDelta*=(-1);
            if (Math.PI*2> angle && angle> (Math.PI)){
                angle+=Math.PI/2;
            }
            else if (Math.PI>angle && angle >0){
                angle-=Math.PI/2;
            }
            else angle-=Math.PI;
        }
        else if( x_tPlusDelta > comuna.getWidth()){
            x_tPlusDelta= comuna.getWidth()-(x_tPlusDelta - comuna.getWidth());
            if (Math.PI*2> angle && angle> (Math.PI)){
                angle-=Math.PI/2;
            }
            else if (Math.PI>angle && angle >0){
                angle+=Math.PI/2;
            }
            else angle+=Math.PI;
        }
        //if (angle>2*Math.PI){angle-=2*Math.PI;}
        y_tPlusDelta=y+speed*Math.sin(angle);
        if(y_tPlusDelta < 0){   // rebound logic
            y_tPlusDelta*=-1;
            if (Math.PI/2> angle && angle> 0){
                angle+=Math.PI/2;
            }
            else if (Math.PI>angle && angle >Math.PI/2){
                angle-=Math.PI/2;
            }
            else angle-=Math.PI;

        }
        else if( y_tPlusDelta > comuna.getHeight()){
            y_tPlusDelta= comuna.getHeight()-(y_tPlusDelta - comuna.getHeight());
            if (Math.PI*3/2> angle && angle> 0){
                angle+=Math.PI/2;
            }
            else if (Math.PI*2>angle && angle >Math.PI*3/2){
                angle-=Math.PI/2;
            }
            else angle+=Math.PI;
        }
    }
    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
    }
}
