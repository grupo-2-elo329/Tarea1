import java.awt.geom.Rectangle2D;

public class Vacunatorio {
    private double VacSize; //el lado
    public double x,y; //el centro

    public Vacunatorio(double VacSize, Comuna comuna){
        this.VacSize = VacSize; //literalmente un cuadrado
        this.x = VacSize/2 + Math.random()*(comuna.getWidth()-VacSize);//para saber donde se setea la wea
        this.y = VacSize/2 + Math.random()*(comuna.getHeight()-VacSize);//para saber donde se setea la wea
    }

    public double getPosx(){
        return x;
    }
    public double getPosy(){
        return y;
    }

    public boolean vacunar(Individuo person, Vacunatorio [] vacs){
        if(person.GetStatus() == 0) {
            double x_pos = person.getPosx();
            double y_pos = person.getPosy();
            for (int i = 0; i < vacs.length; i++) {
                if (x_pos > vacs[i].getPosx() - VacSize / 2 && y_pos > vacs[i].getPosy() - VacSize / 2) {
                    if (x_pos < vacs[i].getPosx() + VacSize / 2 && y_pos < vacs[i].getPosy() + VacSize / 2) {
                        return true;
                    }
                }
            }
        }
        return false;
    }





}