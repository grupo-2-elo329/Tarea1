import java.awt.geom.Rectangle2D;

public class Comuna {
    Individuo[] ListaIndividuos;
    Vacunatorio[] ListaVacunatorios;
    private double VacTime;
    boolean vacunado;
    private int contadorVacunatorios=0;
    private double p0, p1, p2, tRec;
    private Individuo person;
    private int contadorIndividuos=0;
    private int numberIndividuos;
    private Rectangle2D territory; // Alternatively: double width, length;


    public Comuna(){
        territory = new Rectangle2D.Double(0, 0, 1000, 1000); // 1000x1000 mÂ²;
    }

    public Comuna(double width, double length, int N, double p0, double p1, double p2, double tR, double VacTime, int NumVac){
        territory = new Rectangle2D.Double(0,0, width, length);
        person=null;
        vacunado = false;
        ListaIndividuos = new Individuo[N];
        numberIndividuos=N;
        ListaVacunatorios = new Vacunatorio[NumVac];
        this.p0=p0;
        this.p1=p1;
        this.p2=p2;
        this.tRec=tR;
        this.VacTime = VacTime;
    }
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    public void setPerson(Individuo person){
        ListaIndividuos[contadorIndividuos]=person;
        contadorIndividuos+=1;
    }

    public void setVacunatorio(Vacunatorio vac){
        ListaVacunatorios[contadorVacunatorios]=vac;
        contadorVacunatorios++;
    }


    public void computeNextState (double delta_t, double distancia, double t, boolean set) {
        for (int i=0; i < numberIndividuos; i++) {
            ListaIndividuos[i].computeNextPos();
            if(set) {
                vacunado = ListaVacunatorios[0].vacunar(ListaIndividuos[i], ListaVacunatorios);
            }
            ListaIndividuos[i].computeNextState(ListaIndividuos, distancia, t, tRec, p0, p1, p2, vacunado);

        }
    }
    public void updateState () {
        for (int i=0; i < numberIndividuos; i++) {
            ListaIndividuos[i].updateState();
        }
    }
    // include others methods as necessary
    public static String getStateDescription(){ return Individuo.getStateDescription();}

    public String getState(int i){return ListaIndividuos[i].getState(); }

    public int getNinfectados(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].GetStatus() == 1){
                j++;
            }
        }
        return j;
    }

    public int getNrecuperados(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].GetStatus() == 2){
                j++;
            }
        }
        return j;
    }

    public int getNsusceptibles(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].GetStatus() == 0){
                j++;
            }
        }
        return j;
    }

    public int getNvacunados(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].GetStatus() == 3){
                j++;
            }
        }
        return j;
    }

}