import java.io.PrintStream;

public class Simulador {
    private Comuna comuna;
    private PrintStream out;

    private Simulador(){ }
    public Simulador (PrintStream output, Comuna comuna){
        out=output;

        this.comuna = comuna;
    }
    private void printStateDescription(){
        String s="time,\t"+Comuna.getStateDescription();
        out.println(s);
    }
    private void printState(double t){
        String s =Math.round(t*100.0)/100.0 + ",\t\t";
        s+= comuna.getNinfectados() + ",\t\t"+ comuna.getNrecuperados() + ",\t\t"+ comuna.getNsusceptibles();
        out.println(s);
    }
    public void ubicarindividuos(int N,int I, double speed, double delta_t, double deltaAngle, double mask){
        int contador=0;
        Individuo persona;
        for (int i=0; i<I;i++) {
            if (contador < Math.round(I*mask)) {
                persona = new Individuo(comuna, speed, deltaAngle,  1, true);
                contador+=1;
            } else {
                persona = new Individuo(comuna, speed, deltaAngle,  1, false);
            }
            this.comuna.setPerson(persona);
        }
        for (int i=I; i<N;i++) {
            if (contador < Math.round((N-I)*mask)) {
                persona = new Individuo(comuna, speed, deltaAngle,  0, true);
                contador+=1;
            } else {
                persona = new Individuo(comuna, speed, deltaAngle,  0, false);
            }
            this.comuna.setPerson(persona);
        }
    }
    /**
     * @param delta_t time step
     * @param endTime simulation time
     * @param samplingTime  time between printing states to not use delta_t that would generate too many lines.
     */
    public void simulate (double delta_t, double endTime, double samplingTime, double distancia) {  // simulate time passing
        double t=0;
        printStateDescription();
        printState(t);
        while (t<endTime) {
            for(double nextStop=t+samplingTime; t<nextStop; t+=delta_t) {
                comuna.computeNextState(delta_t, distancia, t); // compute its next state based on current global state
                comuna.updateState();            // update its state
            }
            printState(t);
        }
    }
}