import java.awt.geom.Rectangle2D;

public class Comuna {
    Individuo[] ListaIndividuos;
    private double p0, p1, p2, tRec;
    private Individuo person;
    private int contadorIndividuos=0;
    private int numberIndividuos;
    private Rectangle2D territory; // Alternatively: double width, length;


    public Comuna(){
        territory = new Rectangle2D.Double(0, 0, 1000, 1000); // 1000x1000 mÂ²;
    }

    public Comuna(double width, double length, int N, double p0, double p1, double p2, double tR){
        territory = new Rectangle2D.Double(0,0, width, length);
        person=null;
        ListaIndividuos = new Individuo[N];
        numberIndividuos=N;
        this.p0=p0;
        this.p1=p1;
        this.p2=p2;
        this.tRec=tR;
    }
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    public void setPerson(Individuo person){
        ListaIndividuos[contadorIndividuos]=person;
        contadorIndividuos+=1;
    }
    public void computeNextState (double delta_t, double distancia, double t) {
        for (int i=0; i < numberIndividuos; i++) {
            ListaIndividuos[i].computeNextPos();
            ListaIndividuos[i].computeNextState(ListaIndividuos, distancia, t, tRec, p0, p1, p2);
        }
    }
    public void updateState () {
        for (int i=0; i < numberIndividuos; i++) {
            ListaIndividuos[i].updateState();
        }
    }
    // include others methods as necessary
    public static String getStateDescription(){ return Individuo.getStateDescription();}

    public String getState(int i){return ListaIndividuos[i].getState(); }

    public int getNinfectados(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].getStatus() == 1){
                j++;
            }
        }
        return j;
    }

    public int getNrecuperados(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].getStatus() == 2){
                j++;
            }
        }
        return j;
    }

    public int getNsusceptibles(){
        int j = 0;
        for(int i=0; i<ListaIndividuos.length; i++){
            if(ListaIndividuos[i].getStatus() == 0){
                j++;
            }
        }
        return j;
    }

}
