
public class Individuo {
    private double x, y, speed, angle, deltaAngle;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private int status,nextstatus; // 0-> sano y no recuperado, 1-> infectado ,2-> sano y recuperado
    private double t_infectado;
    private int n_infectados;
    private boolean mask;

    public Individuo (Comuna comuna, double speed, double deltaAngle, int status, boolean mask){
        x=Math.round(Math.random()*comuna.getWidth());
        y=Math.round(Math.random()*comuna.getHeight());
        angle = Math.random()*2*Math.PI;
        this.comuna=comuna;
        this.speed=speed;
        this.deltaAngle=deltaAngle;
        this.status = status;
        this.nextstatus=status;
        t_infectado=0;
        this.mask=mask;
    }

    public static String getStateDescription(){
        return "Infectados,\tRecuperados,\tSusceptibles";
    }
    public String getState() {
        return x + ",\t" + y;
    }
    public double getPosx(){
        return x;
    } //da la pos en x
    public double getPosy(){
        return y;
    } //da la pos en y
    public int getStatus(){
        return status;
    }
    public int getNextStatus(){return nextstatus;}
    public boolean getMask(){return mask;}
    public void computeNextPos() {
        double r=Math.random();
        double alpha=deltaAngle*(1-2*Math.round(r));//suma o resta aleatoriamente el deltaAngle
        angle+= alpha;
        if (angle>2*Math.PI){
            angle-=2*Math.PI;
        }
        x_tPlusDelta=x+speed*Math.cos(angle);
        if(x_tPlusDelta < 0){   // rebound logic
            x_tPlusDelta*=(-1);
            if (Math.PI*2> angle && angle> (Math.PI)){
                angle+=Math.PI/2;
            }
            else if (Math.PI>angle && angle >0){
                angle-=Math.PI/2;
            }
            else angle-=Math.PI;
        }
        else if( x_tPlusDelta > comuna.getWidth()){
            x_tPlusDelta= comuna.getWidth()-(x_tPlusDelta - comuna.getWidth());
            if (Math.PI*2> angle && angle> (Math.PI)){
                angle-=Math.PI/2;
            }
            else if (Math.PI>angle && angle >0){
                angle+=Math.PI/2;
            }
            else angle+=Math.PI;
        }
        y_tPlusDelta=y+speed*Math.sin(angle);
        if(y_tPlusDelta < 0){   // rebound logic
            y_tPlusDelta*=-1;
            if (Math.PI/2> angle && angle> 0){
                angle+=Math.PI/2;
            }
            else if (Math.PI>angle && angle >Math.PI/2){
                angle-=Math.PI/2;
            }
            else angle-=Math.PI;

        }
        else if( y_tPlusDelta > comuna.getHeight()){
            y_tPlusDelta= comuna.getHeight()-(y_tPlusDelta - comuna.getHeight());
            if (Math.PI*3/2> angle && angle> 0){
                angle+=Math.PI/2;
            }
            else if (Math.PI*2>angle && angle >Math.PI*3/2){
                angle-=Math.PI/2;
            }
            else angle+=Math.PI;
        }
        x_tPlusDelta=Math.round(x_tPlusDelta*100.0)/100.0;
        y_tPlusDelta=Math.round(y_tPlusDelta*100.0)/100.0;
    }
    public void computeNextState(Individuo[] lista, double distancia, double  t_actual,double t_recuperacion, double proba0, double proba1, double proba2){
        if(status==1){
            //System.out.println("entre");
            if (this.recuperacion(t_recuperacion,t_actual)){nextstatus=2;}
        }
        if(this.Infeccion(lista, t_actual, distancia, proba0, proba1, proba2)){
            //System.out.println("entre");
            nextstatus=1;}
    }
    private boolean Infeccion(Individuo[] lista, double t, double d, double p0, double p1, double p2){
        if (status == 0) {
            int c, m;
            int i;
            c = this.radar(lista,x_tPlusDelta,y_tPlusDelta,d);
            if(c >0){
                m = this.radarmask(lista,x_tPlusDelta,y_tPlusDelta,d);
                for(i=0 ; i<m; i++){
                    if(mask){
                        if(Math.random() < p2) {
                            t_infectado=t;
                            return true;
                        }
                    }
                    else{
                        if(Math.random() < p1) {
                            t_infectado=t;
                            return true;
                        }
                    }
                }
                for(i=m; i<c; i++) {
                    if (Math.random() < p0) {
                        t_infectado = t;
                        return true;
                    }
                }

            }
        }
        return false;
    }
    /*
 _x: Poscicion x de un individuo sano
 _y: Poscicion y de un individuo sano
 d: Radio de contagio
 */
    private int radar(Individuo[] lista, double x, double y, double d){
        double x_prima = x;
        double y_prima = y;
        double diff_x, diff_y;
        int j=0;
        for(int i = 0; i < lista.length; i++){
            if(lista[i].getStatus() == 1){
                diff_x=lista[i].getPosx()-x_prima;
                diff_y=lista[i].getPosy()-y_prima;
                if(diff_x*diff_x+diff_y*diff_y <= d*d) {
                    j++;
                }
            }
        }
        return j;
    }
    private int radarmask(Individuo[] lista, double x, double y, double d){
        double x_prima = x;
        double y_prima = y;
        double diff_x, diff_y;
        int j=0;
        for(int i = 0; i < lista.length; i++){
            if(lista[i].getStatus() == 1){
                diff_x=lista[i].getPosx()-x_prima;
                diff_y=lista[i].getPosy()-y_prima;
                if(diff_x*diff_x+diff_y*diff_y <= d*d) {
                    if(lista[i].getMask()){
                        j++;
                    }
                }
            }
        }
        return j;
    }
    public void updateState(){
        x=x_tPlusDelta;
        y=y_tPlusDelta;
        status=nextstatus;
    }
    private boolean recuperacion(double I_time, double t){
        if(t-t_infectado>=I_time){return true;}
        else {return false;}
    }
}
