# [ELO329] [Tarea 1: Simulando la Evoluci�n de una Pandemia](https://gitlab.com/grupo-2-elo329/Tarea1)

## Integrantes
* [Jared Soto](https://gitlab.com/Jared677)
* [Philippe Pastene](https://gitlab.com/Philippe_)
* [Cristobal Gonzalez](https://gitlab.com/Cristobal.Gonzalezs)
* [Ignacio Gaete](https://gitlab.com/IgnacioGaete)

## Objetivo General

Esta tarea busca practicar la orientaci�n a objeto en una situaci�n inspirada en una pandemia ti Covid-19. De ninguna manera la intenci�n es obtener resultados sobre c�mo deber�a evolucionar una pandemia real. Adem�s, otros objetivos del proyecto fueron:

* Modelar objetos reales como objetos de software.
* Ejercitar la creaci�n y extensi�n de clases dadas para satisfacer nuevos requerimientos.
* Reconocer clases y relaciones entre ellas en c�digos fuentes Java.
* Ejercitar la compilaci�n y ejecuci�n de programas en lenguaje Java desde una consola de comandos.
* Ejercitar la configuraci�n de un ambiente de trabajo para desarrollar aplicaciones en lenguaje Java.
* Manejar proyectos v�a [GIT](https://git-scm.com/).
* Conocer el formato .csv y su importaci�n hacia una planilla electr�nica.
* Ejercitar la preparaci�n y entrega de resultados de software (creaci�n de makefiles, readme, documentaci�n).
* Familiarizaci�n con desarrollos "iterativos" e "incrementales".

## Descripci�n de la Tarea

Se considera un n�mero dado de **N** individuos representados por la clase *Individuo* dispersos que se mueven aleatoriamente y de manera confinada en una regi�n plana rectangular llamada *Comuna* durante un tiempo **T** (tiempo de simulaci�n). De los **N** individuos iniciales, **S** es el n�mero de individuos susceptibles a infectarse e **I** es el n�mero de individuos ya infectados. En este ejercicio los individuos infectados pasan a estado **R** (recuperados) al cabo de **I_time** segundos.

La *Comuna* es rectangular de dimensiones (**width**,**length**) y los movimientos de los individuos son aleatorios con rapidez **Speed**. La direcci�n **theta** de la velocidad var�a cada **dt** a un valor aleatorio tomado de entre **theta-dTheta** y **theta+dTheta**. Cuando un *Individuo* tiende a salir de la *Comuna*, �ste cambia su direcci�n como si estuviera rebotando en el borde de �sta.

Cuando un *Individuo* susceptible de infectarse se acerca a una distancia inferior a **d** [m] de uno infectado, existe una probabilidad P de que adquiera la infecci�n por cada segundo en contacto cercano. Esta probabilidad de contagio depende de si ningun (P=**p0**), solo uno (P=**p1**) o ambos (P=**p2**) usan mascarilla.

Finalmente, en la *Comuna* se activan **NumVac** *Vacunatorios* a los **VacTime** segundos de iniciada la simulaci�n. Los *Vacunatorios* son representados como zonas cuadradas fijas de lado **VacSize** y ubicados aleatoriamente en la comuna. Cuando un *Individuo* susceptible ingresa al �rea de un *Vacunatorio*, �ste es vacunado y deja de ser susceptible a la infecci�n. *Individuos* infectados o recuperados no son vacunados en esta simulaci�n.

## [Stage 1](https://gitlab.com/grupo-2-elo329/Tarea1/-/tree/master/Stage1): Individuo se mueve aleatoriamente
En esta etapa no hay vacunatorios, no se usa mascarilla y solo existe un *Individuo* que se mueve aleatoriamente en la *Comuna*. Las clases utilizadas aqu� son *Stage1*, *Simulador*, *Comuna* e *Individuo*.

La clase *Stage1* contiene el m�todo main, crea una instancia de *Comuna*, una de *Individuo* y una de *Simulador* a partir del archivo de entrada. Esta clase es la encargada de correr la simulaci�n, manejando el avance del tiempo e imprimiendo los valores de salida del programa. La clase *Comuna* define el territorio y contiene al �nico individuo que se mueve en ella. La clase *Individuo* define la ubicaci�n de los individuos dentro de la comuna y el movimiento de �stos dentro de ella.

## [Stage 2](https://gitlab.com/grupo-2-elo329/Tarea1/-/tree/master/Stage2): Varios Individuos se mueven y se contagian
Esta etapa es similar a la previa (sin vacuna, sin mascarilla), excepto que se crean **N** individuos los cuales son almacenados en un arreglo. La clase simulador crea y ubica **I** individuos infectados y (**N**-**I**) individuos susceptibles de infectarse. La clase *Individuo* se completa para reflejar la interacci�n entre individuos. Como salida se imprime por pantalla y en columnas separadas **t**, **I**, **R**, **S** (tiempo, infectados, recuperados y susceptibles).

## [Stage 3](https://gitlab.com/grupo-2-elo329/Tarea1/-/tree/master/Stage3): Alguno de los Individuos pueden usar mascarilla
Esta etapa modifica la forma como los individuos se pueden contagiar dependiendo del uso de mascarilla, implementada como un atributo extra en la clase *Individuo*. La salida tiene la misma estructura de la etapa anterior, es decir, **t**, **I**, **R**, **S**.

## [Stage 4](https://gitlab.com/grupo-2-elo329/Tarea1/-/tree/master/Stage4): Se crean Vacunatorios que inmunizan a Individuos de ser contagiados
En esta �ltima etapa se logra incorporar todas las funcionalidades pedidas, es decir, individuos movi�ndose aleatoriamente en la comuna, unos con mascarillas y otros no, se contagian entre ellos y adem�s al cabo de un tiempo **VacTime** se generan **NumVac** vacunatorios los cuales vacunan aquellos individuos susceptibles que esten dentro de su regi�n. La salida tiene la siguiente estructura: **t**, **V**, **I**, **R**, **S** (tiempo, vacunados, infectados, recuperados y susceptibles).

## Archivos de entrada y salida
Cada etapa requiere de un archivo de entrada (**entrada.txt**) donde se establecen los par�metros de simulaci�n. La estructura de este archivo es la siguiente:

**T**<*espacio*>**N**<*espacio*>**I**<*espacio*>**I_Time**

**width**<*espacio*>**length**

**speed**<*espacio*>**dt**<*espacio*>**dTheta**

**d**<*espacio*>**M**<*espacio*>**p0**<*espacio*>**p1**<*espacio*>**p2**

**NumVac**<*espacio*>**VacSize**<*espacio*>**VacTime**<*espacio*>

Si bien la estructura es r�gida, el valor de cada par�metro puede ser cambiado sin ningun problema (**deben estar todos definidos incluso si su valor fuera cero**). Por otro lado, cada etapa tambi�n esta dise�ada para que su salida por pantalla sea compatible con archivos de tipo .csv en donde mediante el uso de Excel se pueden generar gr�ficos interesantes de las variables en cuesti�n (en la secci�n siguiente se detalla el c�mo generar este tipo de documento).

## Sobre como compilar y ejecutar cada etapa
En cada etapa existe un archivo *makefile* encargado de manejar la compilaci�n, ejecuci�n y limpieza de archivos .class generados. Por ejemplo, para compilar se utiliza `$make`, para correr el progrma `$make run` (si se quiere sobrescribir el archivo de salida en vez de mostrar por pantalla se debe escribir `$make run > sources/salida.csv`)
 y para limpiar los archivos .class se ejecuta `$make clean`.

## Dificultades encontradas al desarrollar la tarea
La primera dificultad fue la parte de coordinaci�n y uso de GIT, ya que se ten�a escaso o nulo conocimiento en un comienzo, por lo que la generaci�n de m�ltiples *commits* de prueba y *branches* desorganizadas fue algo recurrente. Adem�s si bien se logr� dominar las funciones b�sicas de esta herramienta, el trabajo remoto es algo que a�n no se est� al 100% acostumbrado.

En lo que respecta a implementaci�n, si bien los objetos a modelar estaban claramente definidos, nos dimos cuenta que existen una infinidad de opciones para representar sus atributos y funcionalidades en c�digo, lo que retras� en ocaciones nuestro avance. Sin embargo, la forma **iterativa** de crear un proyecto (que era uno de los objetivos de esta tarea) nos ayud� bastante, permitiendo reutilizar c�digo de etapas anteriores para luego simplemente extender funcionalidades, agregar clases, atributos o m�todos que fueran necesarios (por ejemplo en *Stage1* se implement� la l�gica del movimiento de un individuo mediante el m�todo *computeNextState()*, el cual simplemente se cambi� de nombre en etapas posteriores a *computeNextPos()* para separarlo de la l�gica de contagio entre individuos implementada con el m�todo *computeNextState()*)
