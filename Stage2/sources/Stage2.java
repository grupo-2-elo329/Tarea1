import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Stage2 {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1Main <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner s = new Scanner(new File(args[0]));
        System.out.println("File: " + args[0]);
        double simulationDuration = s.nextDouble();
        System.out.println("Simulation time: " + simulationDuration);
        int numberIndividuos = s.nextInt();
        int numberInfectado = s.nextInt();
        double tiempoRecuperacion = s.nextDouble();
        double comunaWidth = s.nextDouble();
        double comunaLength = s.nextDouble();
        double speed = s.nextDouble();
        double delta_t = s.nextDouble();
        double deltaAngle = s.nextDouble();
        double distancia = s.nextDouble();
        double mascarilla = s.nextDouble();
        double p0 = s.nextDouble();
        double samplingTime = 1.0;  // 1 [s]
        Comuna comuna = new Comuna(comunaWidth, comunaLength, numberIndividuos, p0, tiempoRecuperacion);
        Simulador sim = new Simulador(System.out, comuna);
        sim.ubicarindividuos(numberIndividuos,numberInfectado, speed, delta_t, deltaAngle);
        sim.simulate(delta_t, simulationDuration, samplingTime, distancia);
        //comento para ver si funciona subirlo-Cristobal
    }
}